<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/inquizarus/phunc/src/functions.php';

$makeArrayExtractor = function(array $data, $fallback) {
    return function(string $key) use ($data, $fallback) {
        return $data[$key] ?? $fallback;
    };
};

$payload = file_get_contents("https://jsonplaceholder.typicode.com/users");
$data = json_decode($payload, true);


$count = filter($data, function($i) {
    return $i["id"] % 3 === 0;
})->each(function($i) use ($makeArrayExtractor) {
    $f = $makeArrayExtractor($i, "unknown");
    echo sprintf("Username: %s, Email: %s", $f("username"), $f("email")) . PHP_EOL;
})->reduce(function($c, $i){
    return $c + 1;
}, 0);

echo sprintf("there are %d users with valid criteria", $count);